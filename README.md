poc-gitlab-runner-cache
==

This the Proof-of-Concept for explorations into the [`cache`][cache] support
for Gitlab CI.

[cache]:https://docs.gitlab.com/ee/ci/yaml/index.html#cache
